FROM centos:latest
MAINTAINER The CentOS Project <cloud-ops@centos.org>

RUN yum -y update && \
 yum -y install epel-release && \
 echo "exclude=postgresql*" >> /etc/yum.repos.d/CentOS-Base.repo && \
 yum --setopt=tsflags=nodocs -y install systemd && \
 yum --setopt=tsflags=nodocs install -y https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-redhat96-9.6-3.noarch.rpm && \
 yum --setopt=tsflags=nodocs install -y postgresql96-server postgresql96-contrib postgresql96-devel postgresql96-docs postgresql96-libs perl-DBD-Pg sudo supervisor pwgen && \
 yum clean all

COPY postgresql-setup /usr/bin/postgresql-setup
COPY supervisord.conf /etc/supervisord.conf
COPY start_postgres.sh /start_postgres.sh

RUN chmod +x /usr/bin/postgresql-setup  && \
 chmod +x /start_postgres.sh

USER postgres
WORKDIR /var/lib/pgsql/
RUN ls -la

#RUN pwd
RUN ln -s  /var/lib/pgsql/9.6/data/ && \
#RUN ls -la /var/lib/pgsql/ && \
#RUN ls -la /var/lib/pgsql/9.6/ && \
 chown postgres:postgres -R /var/lib/pgsql/ && \
#RUN /usr/pgsql-9.6/bin/initdb
 su -l postgres -c "/usr/pgsql-9.6/bin/initdb --pgdata='/var/lib/pgsql/9.6/data/' --auth='ident'" >> /var/lib/pgsql/initdb.log 2>&1 < /dev/null && \
 sed -i -e 's/local   all/#local   all/g' /var/lib/pgsql/9.6/data/pg_hba.conf && \
 sed -i -e 's/host    all/#host    all/g' /var/lib/pgsql/9.6/data/pg_hba.conf && \
 sed -i -e 's/md5/trust/g' /var/lib/pgsql/9.6/data/pg_hba.conf && \
 echo "host    all             all             0.0.0.0/0               trust" >> /var/lib/pgsql/data/pg_hba.conf && \
 echo "local   all             all                                     trust" >> /var/lib/pgsql/data/pg_hba.conf

ADD postgresql.conf /var/lib/pgsql/data/postgresql.conf

RUN chown -v postgres.postgres /var/lib/pgsql/data/postgresql.conf


WORKDIR /var/lib/pgsql/
 CMD ["/bin/bash", "usr/pgsql-9.6/bin/pg_ctl","-D","/var/lib/pgsql/9.6/data/","-l","logfile","restart"]
# su -l postgres -c "/usr/pgsql-9.6/bin/pg_ctl -D /var/lib/pgsql/9.6/data/ -l logfile start" >> /var/lib/pgsql/pg_ctl.log 2>&1 < /dev/null

WORKDIR /root/

VOLUME ["/var/lib/pgsql"]

EXPOSE 5432

CMD ["/bin/bash", "/start_postgres.sh"]
